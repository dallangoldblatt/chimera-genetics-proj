import sys
import os
import numpy as np
import chimera
import time

#first you should open a command line in the directory where you're input files are (if you want to use %cd%)
#syntax in command line (windows) should be: chimera <--nogui> --script "mutate.py %cd% <false/true>"

#false means it will not generate a new .pdb file and use 1kx5.pdb in the root directory (faster)
#true means it will overrite the current .pdb file with a fresh modified 1xk5.pdb in the root directory
#    TODO: change true to create 1kx5-n.pdb file where n is an integer so no overwriting files

def main():
    output(time.strftime('%c'))
    output('Using ' + sys.argv[1] + ' as root directory.')
    _, input_seq = read_fasta(open(os.path.join(sys.argv[1], 'input.fasta'))).next()
    output('Inputted sequence: ' + input_seq)
    complementary_strand = get_complementary_strand(input_seq)
    output('Complement strand: ' + complementary_strand)
    chimera.runCommand("~scene all")
    if (sys.argv[2]):
        chimera.runCommand('open 1kx5')
    else:
        chimera.runCommand('open ' + os.path.join(sys.argv[1], '1kx5.pdb'))
    output('Opened model 1kx5')
    for i in range(-73,74):
        output('Ran command: ' + 'swapna ' + input_seq[i+73] + ' :' + str(i) + '.i')
        chimera.runCommand('swapna ' + input_seq[i+73] + ' :' + str(i) + '.i')
        output('Ran command: ' + 'swapna ' + complementary_strand[i+73] + ' :' + str(i*-1) + '.j')
        chimera.runCommand('swapna ' + complementary_strand[i+73] + ' :' + str(i*-1) + '.j')
    chimera.runCommand('write format pdb 0 1kx5.pdb') #still trying to fix this, doesnt work as intended
    chimera.runCommand('close all')

def output(entry):
    output_file = open(os.path.join(sys.argv[1], 'output_logs.txt'), 'a')
    output_file.write('\n' + entry)
    output_file.close();

def get_complementary_strand(strand):
    out = ''
    for x in range(0,len(strand)):
        nucleotide = strand[x]
        if (nucleotide == 'A'):
            out = out + 'T'
        elif (nucleotide == 'T'):
            out = out + 'A'
        elif (nucleotide == 'G'):
            out = out + 'C'
        elif (nucleotide == 'C'):
            out = out + 'G'
    return out

def read_fasta(fp): #taken from biopython (i wanted to minimize dependencies for easy runtime, sorry biopython creator)
    name, seq = None, []
    for line in fp:
        line = line.rstrip()
        if line.startswith(">"):
            if name: yield (name, ''.join(seq))
            name, seq = line, []
        else:
            seq.append(line)
    if name: yield (name, ''.join(seq))

if __name__ == '__main__':
    main();